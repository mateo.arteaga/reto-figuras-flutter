import 'dart:math';

import 'package:flutter/material.dart';

class TriangleWidget extends StatelessWidget {
  const TriangleWidget({Key? key, required this.sideSize}) : super(key: key);

  final double sideSize;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: 300,
        height: 300,
        child: CustomPaint(
          painter: TrianglePainter(sideSize: sideSize),
        ),
      ),
    );
  }
}

class TrianglePainter extends CustomPainter {
  TrianglePainter({required this.sideSize}) : super();
  final double sideSize;

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = Colors.green
      ..strokeWidth = 5
      ..style = PaintingStyle.stroke;

    final path = Path();
    final h = sin(pi / 3) * sideSize;
    final initialPosition = Offset(size.width / 2, (size.height / 2) - (h / 2));
    final movementInX = sideSize / 2;
    final lineTo1 =
        Offset(initialPosition.dx + movementInX, initialPosition.dy + h);
    final lineTo2 =
        Offset(initialPosition.dx - movementInX, initialPosition.dy + h);

    path.moveTo(initialPosition.dx, initialPosition.dy);
    path.lineTo(lineTo1.dx, lineTo1.dy);
    path.lineTo(lineTo2.dx, lineTo2.dy);
    path.close();

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
