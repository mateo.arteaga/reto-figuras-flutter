import 'dart:html';

import 'package:flutter/material.dart';
import '/ui/pages/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      // themeMode: ThemeMode.dark,
      // darkTheme: ThemeData(
      //   textTheme: Theme.of(context).textTheme.apply(
      //         bodyColor: Color(0xFFededef),
      //         displayColor: Colors.white,
      //         decorationColor: Colors.white,

      //       ),
      //   scaffoldBackgroundColor: const Color(0xFF161618),
      //   primarySwatch: Colors.blue,
      // ),
      home: const HomePage(),
    );
  }
}
