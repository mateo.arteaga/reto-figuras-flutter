import 'dart:math';

import 'package:flutter/material.dart';

class HexagonWidget extends StatelessWidget {
  const HexagonWidget({Key? key, required this.sideSize}) : super(key: key);

  final double sideSize;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: 300,
        height: 300,
        child: Transform.rotate(
          angle: 30 * pi / 180,
          child: CustomPaint(
            painter: HexagonPainter(sideSize: sideSize),
          ),
        ),
      ),
    );
  }
}

class HexagonPainter extends CustomPainter {
  HexagonPainter({required this.sideSize}) : super();
  final double sideSize;

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = Colors.green
      ..strokeWidth = 5
      ..style = PaintingStyle.stroke;

    final path = Path();
    final p0 = Offset(size.width / 2, size.height / 2);
    final movementInX = cos(pi / 6) * sideSize;
    final movementInY = sin(pi / 6) * sideSize;

    final p1 = Offset(p0.dx, p0.dy - sideSize);
    final p2 = Offset(p1.dx + movementInX, p1.dy + movementInY);
    final p3 = Offset(p2.dx, p2.dy + sideSize);
    final p4 = Offset(p3.dx - movementInX, p3.dy + movementInY);
    final p5 = Offset(p4.dx - movementInX, p4.dy - movementInY);
    final p6 = Offset(p5.dx, p5.dy - sideSize);
    final p7 = Offset(p6.dx + movementInX, p6.dy - movementInY);

    path.moveTo(p0.dx, p0.dy);
    path.moveTo(p1.dx, p1.dy);
    path.lineTo(p2.dx, p2.dy);
    path.lineTo(p3.dx, p3.dy);
    path.lineTo(p4.dx, p4.dy);
    path.lineTo(p5.dx, p5.dy);
    path.lineTo(p6.dx, p6.dy);
    path.close();

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
