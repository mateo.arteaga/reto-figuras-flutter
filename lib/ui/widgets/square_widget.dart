import 'package:flutter/material.dart';

class SquareWidget extends StatelessWidget {
  const SquareWidget({Key? key, required this.sideSize}) : super(key: key);
  final double sideSize;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: 300,
        height: 300,
        child: CustomPaint(
          painter: SquarePainter(sideSize: sideSize),
          // child: Text(
          //   "Custom Paint",
          //   style: TextStyle(fontSize: 30, fontStyle: FontStyle.italic),
          // ),
        ),
      ),
    );
  }
}

class SquarePainter extends CustomPainter {
  SquarePainter({required this.sideSize}) : super();
  final double sideSize;

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = Colors.blue
      ..strokeWidth = 5
      ..style = PaintingStyle.stroke;

    final a = Offset(
        (size.width / 2) - (sideSize / 2), (size.width / 2) - (sideSize / 2));
    final b = Offset(a.dx + sideSize, a.dy + sideSize);

    final rect = Rect.fromPoints(a, b);

    canvas.drawRect(rect, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
