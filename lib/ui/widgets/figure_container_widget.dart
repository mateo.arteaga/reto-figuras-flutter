import 'package:figuras/ui/widgets/square_widget.dart';
import 'package:figuras/ui/widgets/triangle_widget.dart';
import 'package:flutter/material.dart';

import 'hexagon_widget.dart';

enum FigureType {
  triangle,
  square,
  hexagon,
}

class FigureContainer extends StatefulWidget {
  const FigureContainer(
      {Key? key, required this.figure, required this.figureTitle})
      : super(key: key);

  final FigureType figure;
  final String figureTitle;

  @override
  State<FigureContainer> createState() => _FigureContainerState();
}

class _FigureContainerState extends State<FigureContainer> {
  late double _inputVal;
  late double _sideSize;

  void manageInput(val) => setState(() {
        _inputVal = double.tryParse(val)!;
      });

  @override
  void initState() {
    super.initState();
    _inputVal = 75;
    _sideSize = _inputVal;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 500,
      height: 460,
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: const Color(0xFF232326), //change for dark mode theming
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          color: Color(0xFF3e3e44),
          width: 2,
        ),
      ),
      child: Column(
        children: <Widget>[
          Text(
            widget.figureTitle,
            style: const TextStyle(
              fontSize: 30,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          SizedBox(
            width: 300,
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    style: const TextStyle(color: Colors.blue),
                    decoration: InputDecoration(
                      labelText: 'Side Size',
                      hintText: _inputVal.toString(),
                    ),
                    keyboardType: TextInputType.number,
                    onChanged: manageInput,
                  ),
                ),
                Expanded(
                  child: TextButton(
                    onPressed: () {
                      setState(() {
                        _sideSize = _inputVal;
                      });
                    },
                    child: const Text('Change'),
                  ),
                ),
              ],
            ),
          ),
          correspondingWidget(widget.figure, _sideSize)
        ],
      ),
    );
  }
}

Widget correspondingWidget(FigureType figure, double sideSize) {
  if (figure == FigureType.triangle) {
    return TriangleWidget(sideSize: sideSize);
  }
  if (figure == FigureType.square) {
    return SquareWidget(sideSize: sideSize);
  }
  if (figure == FigureType.hexagon) {
    return HexagonWidget(sideSize: sideSize);
  }
  return Container();
}
