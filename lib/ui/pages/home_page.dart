import 'package:flutter/material.dart';
import '/ui/widgets/figure_container_widget.dart';
import '/ui/widgets/hexagon_widget.dart';
import '/ui/widgets/triangle_widget.dart';
import '/ui/widgets/square_widget.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF161618),
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Wrap(
            direction: Axis.vertical,
            spacing: 20,
            children: const [
              SizedBox(height: 20),
              FigureContainer(
                  figure: FigureType.triangle, figureTitle: 'Triangle'),
              FigureContainer(figure: FigureType.square, figureTitle: 'Square'),
              FigureContainer(
                  figure: FigureType.hexagon, figureTitle: 'Hexagon'),
            ],
          ),
        ),
      ),
    );
  }
}
